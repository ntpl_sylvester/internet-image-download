package in.forsk.internet_image_download;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.androidquery.AQuery;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    Button loadBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.imageView1);
        loadBtn = (Button) findViewById(R.id.button1);

        loadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog pd =  new ProgressDialog(MainActivity.this);
                pd.setMessage("Loading");
                new AQuery(MainActivity.this).progress(pd).id(imageView).image("http://iiitkota.forsklabs.in/appLogo.png");
            }
        });
    }
}
