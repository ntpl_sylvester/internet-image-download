package in.forsk.internet_image_download;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;

/**
 * Created by Saurabh on 5/13/2016.
 */
public class ImageLoaderAsyncTask extends AsyncTask<String, Integer, Bitmap> {

    Context context;
    ImageView imageView;
    String url = "";

    ProgressDialog pd;


    public ImageLoaderAsyncTask(Context context, String url, ImageView imageView) {
        this.context = context;
        this.url = url;
        this.imageView = imageView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pd = new ProgressDialog(context);
        pd.setMessage("Loading Image..");
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected Bitmap doInBackground(String... params) {

        InputStream in = Utils.openHttpConnection(url);
        Bitmap bmp = BitmapFactory.decodeStream(in);

        for (int i = 1; 10 > i; i++) {
            try {
                onProgressUpdate(i);
                Thread.sleep(500);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return bmp;
    }

    @Override
    protected void onProgressUpdate(final Integer... values) {
        super.onProgressUpdate(values);

        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final int progress = values[0] * 10;
                pd.setMessage("Loading Image  " + progress + "%..");
            }
        });


    }


    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        if (result != null)
            imageView.setImageBitmap(result);

        if (pd != null)
            pd.dismiss();
    }


}